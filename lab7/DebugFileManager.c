/**
 * This file provides the implementation for writing and reading binary files for the memory debugger.
 * @file DebugFileManager.c
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/31/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "DebugFileManager.h"

/**
 * Writes the memory at a starting address for a specified length to a specified file
 * @param input - string of <filename> <startingAddress> <lengthOfBytesToWRite>
 */
void handleFileWrite(char * input){

	if (input != NULL) {

		char * filename;
		char * addressStr;
		char * lengthStr;

		//get parts on input ensuring they aren't null
		if((filename = strtok(input," ")) != NULL && (addressStr = strtok(NULL," ")) != NULL && (lengthStr = strtok(NULL," ")) != NULL) {

			FILE * file = fopen(filename, "wb");

			//parse address and length
			unsigned int address = strtoul(addressStr, NULL, 0);
			int length = strtoul(lengthStr, NULL, 0);

			//print bytes to file for length specified
			for(int i = 0; i < length; i++) {
				fwrite((void*)(address+i), sizeof(char), 1, file);
			}

			fclose(file);

		}else {
			printf("Invalid arguments\n\tWRITE <filename> <startingAddress> <bytesToWrite>\n");
		}
	}
}

/**
 * Writes the bytes from a file to memory starting a specified address
 * @param input <filename> <startingAddress>
 */
void handleFileRead(char * input){

	if (input != NULL) {

		char * filename;
		char * addressStr;

		//get parts on input ensuring they aren't null
		if((filename = strtok(input," ")) != NULL && (addressStr = strtok(NULL," ")) != NULL) {

			//get address
			unsigned int address = strtoul(addressStr, NULL, 0);

			FILE * file = fopen(filename, "rb");
			unsigned int nextByte;

			//get first byte
			nextByte = fgetc(file);

			//until end of file
			while(nextByte != EOF) {

				//put next byte in file into memory
				*(int*)(address++) = nextByte;

				//get next byte
				nextByte = fgetc(file);
			}

			fclose(file);

		}else {
			printf("Invalid arguments\nREAD <filename> <startingAddressToPrint>\n");
		}
	}
}



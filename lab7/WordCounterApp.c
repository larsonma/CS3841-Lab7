/**
 * This file contains the main program flow for the word counter app. This file makes use of many
 * different libraries.
 * @file WordCounterApp.c
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#include "WordCounterApp.h"
#include "Trim.h"
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "words.h"
#include "linkedlist.h"
#include "DebugManager.h"

static void processWord(struct wordList* wordList,char* text);
static int readAndProcessFile(struct wordList* wordList,char* filename);
static void printResults(char* mode, struct wordList* wordList);

const int MAX_WORD_LENGTH = 32;

/**
 * This is the main program flow for counting words in a file. It initializes a word list, reads and
 * processes the file, and prints the results.
 * @param argc	The number of command line arguments.
 * @param argv	Comand line arguments[./lab3 <W or C> <path to file>]
 * @return	EXIT_ERROR is unsucessful, EXIT_SUCCESS if successful.
 */
int main(int argc, char* argv[]){
	int status = EXIT_ERROR;

	//check that there are 3 arguments.
	if(argc == 3){
		struct wordList* myWordList = malloc(sizeof(struct wordList));

		//initalize word list, read file, print results.
		initWordList(myWordList);

		status = readAndProcessFile(myWordList,argv[2]);
		if(status == EXIT_SUCCESS){
			//Before printing, enter debugging mode
			startDebugger();
			printf("Starting address of word list: %p\r\n", myWordList);
			obtainAndPerformNextAction();
			printResults(argv[1], myWordList);
		}

		cleanup(myWordList);

		free(myWordList);

	}else{
		printf("Usage: <W or C> <path to file>\r\n");
	}

	return status;

}

/**
 * This function prints the results of the word count to the console. A command line
 * argument chooses the mode.
 * @param mode		the mode to display, W for word followed by count, C for count followed by word.
 * @param wordList		the word list to print results for.
 */
static void printResults(char* mode, struct wordList* wordList) {
	if (mode != NULL && wordList != NULL) {

		for (int i = 0; i < ll_size(wordList->words); i++) {
			struct wordData* word = getWord(wordList, i);
			if (strcmp(mode, "W") == 0) {
				printf("%s - %d\r\n", word->word, word->count);
			} else if (strcmp(mode, "C") == 0) {
				printf("%d - %s\r\n", word->count, word->word);
			} else {
				printf("%s is not a valid print format",mode);
				break;
			}
		}
	}
}

/**
 * This function processes a word before adding it to a word list. Words are stripped of special
 * characters and then changed to upper case.
 * @param wordList		word list to add the text to.
 * @param text		the text to add to the word list.
 */
static void processWord(struct wordList* wordList, char* text){
	char* word = text;

	char* strippedWord = stripNonText(word);
	char* upperCapsWord = toCaps(strippedWord);
	addWord(wordList,upperCapsWord);

	free(strippedWord);
	free(upperCapsWord);
}

/**
 * This function reads a file word by word, and calls the processWord function for each
 * word. If any file reading errors occur, this function returns a failure code.
 * @param wordList		The word list to add new words to.
 * @param filename	The filename for the file containing words to be read.
 * @return			EXIT_SUCCESS if successful, EXIT_FAILURE otherwise.
 */
static int readAndProcessFile(struct wordList* wordList, char* filename){
	FILE* fp;
	int returnStatus;
	bool fileDone = false;

	//open the file. If The filename does not exist, indicate that illegal parameters were passed in.
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("Invalid params");
		returnStatus = EXIT_ERROR;
		fileDone = true;
	}

	int returnValue = 0;

	while (!fileDone) {
		char word[MAX_WORD_LENGTH];
		returnValue = fscanf(fp, "%31s ", word);

		//handle each case for reading words.
		if (returnValue == 1) {
			processWord(wordList,word);
		} else if (errno != 0) {
			printf("File scanning failed with an error");
			returnStatus = EXIT_ERROR;
			fileDone = true;
		} else if (returnValue == EOF) {
			returnStatus =  EXIT_SUCCESS;
			fileDone = true;
		} else {
			printf("No match to string");
			//return returnStatus = EXIT_ERROR;
			fileDone = true;
		}
	}
	fclose(fp);
	return returnStatus;
}

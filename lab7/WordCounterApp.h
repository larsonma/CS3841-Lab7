/**
 * This file contains interface for the WordCounterApp.
 * @file WordCounterApp.h
 * @author Mitchell Larson
 * @date 9/26/2017
 */


#ifndef SRC_WORDCOUNTERAPP_H_
#define SRC_WORDCOUNTERAPP_H_

//result codes.
#define EXIT_ERROR 1
#define EXIT_SUCCESS 0

#endif /* SRC_WORDCOUNTERAPP_H_ */

/**
 * This file contains the interface for the linked list file.
 * Complete the comments to be compliant with Doxygen practices.
 * @file linkedlist.h
 * @author Your Name
 * @date
 */
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdbool.h>
#include <stdint.h>

struct  listNode {
  void* data;
  uint32_t dataSize;
  struct listNode* nextNode;
  struct listNode* prevNode;
};

struct linkedList
{
  struct listNode* head;
  struct listNode* tail;
  uint32_t size;

};

struct linkedListIterator
{
  struct listNode* current;
};

/**
 * Initializes list, setting all parameters to NULL
 * @param list - List to intialize
 */
void ll_init(struct linkedList* list);

/**
 * Add an element to the end of the linked list
 * @param list - List to add to
 * @param object - Object to add
 * @param size - Data size of object in bytes
 * @return - Return true if successful
 */
bool ll_add(struct linkedList* list, const void* object, uint32_t size);

/**
 * Add and element to list at specified index
 * @param list - List to add to
 * @param object - Pointer to object to add
 * @param size - Data size of object in bytes
 * @param index - Index of list to add at
 * @return - Return true if successful
 */
bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size, uint32_t index);

/**
 * Remove element from list
 * @param list - List to remove element from
 * @param index - Index of element to remove
 * @return - Return true if successful
 */
bool ll_remove(struct linkedList* list, uint32_t index);

/**
 * Get the data from list at certain index
 * @param list - List to retrieve data from
 * @param index - Index of data to retrieve
 */
void* ll_get(struct linkedList* list, uint32_t index);

/**
 * Clear specified list, freeing all memory allocated data ni process
 * @param list - List to clear
 */
void ll_clear(struct linkedList* list);

/**
 * Return size of list
 * @param list - List to return size of
 * @return - 32 bit integer indicating size of list
 */
uint32_t ll_size(struct linkedList* list);

/**
 * Returns iterator for list pointing at head
 * @param list - List for iterator to point to
 * @return - Iterator
 */
struct linkedListIterator* ll_getIterator(struct linkedList* list);

/**
 * Indicates whether the iterator has a next element
 * @param iter - Iterator to check
 * @return - Returns true if next element exists
 */
bool ll_hasNext(struct linkedListIterator* iter);

/**
 * Returns the data at the node pointed to by iterator
 * @param iter - Iterator from which data is to be extracted
 */
void* ll_next(struct linkedListIterator* iter);










#endif

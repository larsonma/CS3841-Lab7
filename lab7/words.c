/*
 * words.c
 *
 *  Created on: Sep 25, 2017
 *      Author: Gunther Huebler
 *
 *  Program handles the counting of words. Adding words for first time creates a new list element
 *  for word and adding words that already exist incements count of list element containing the word.
 */

#include "words.h"
#include "linkedlist.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Method takes in a pointer to a word list and initializes it by allocating space
 * for the linked list within it then setting the word count to 0
 * @param list - Pointer to word list structure
 */
void initWordList(struct wordList* list) {

	if(list != NULL) {
		//allocate a linked list
		list->words = malloc(sizeof(struct linkedList));

		//initialize that list
		ll_init(list->words);

		//set word count to 0
		list->wordCount = 0;
	}
}

/**
 * Method takes in a word list and pointer to word and adds the word to the list
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to add
 */
void addWord(struct wordList * list, char * word) {

	//ensure neither list nor word is null
	if(list != NULL && word != NULL) {

		//find the word in the list and hold the structure returned in wordData
		struct wordData* wordData = findWord(list, word);

		//if word data isnt null, then word has already occurred, increment count
		if(wordData != NULL) {
			wordData->count++;
		}
		//if word data is null then word hasnt occurred
		else {

			//allocate space for new wordData structure
			wordData = malloc(sizeof(struct wordData));

			//set initial count to 1
			wordData->count = 1;

			//copy string data into new wordData
			strcpy(wordData->word, word);

			//add new wordData structure to list
			ll_add(list->words, wordData, sizeof(struct wordData));

			//free wordData (linked list creates a copy)
			free(wordData);
		}

		//increment total count of words
		list->wordCount++;
	}
}

/**
 * Method takes a pointer to a list and a character pointer and returns the wordData structure
 * located in the list which contains the word defined by the character pointer
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to find the data structure of
 * @return - wordData structure for specified word
 */
struct wordData* findWord(struct wordList * list, char * word) {

	//ensure parameters arent null, return null if they are
	if(list == NULL || word == NULL) {
		return NULL;
	}

	//get iterator for wordDatas stored in list and create temp variable to hold them
	struct linkedListIterator* iter = ll_getIterator(list->words);
	struct wordData* temp;

	//iterate through list of wordDatas
	while(ll_hasNext(iter)) {
		temp  = (struct wordData*)ll_next(iter);

		//if wordData has word were looking for then free the iterator and return the wordData structure
		if(!strcmp(temp->word, word)) {
			free(iter);
			return temp;
		}
	}

	//if the word was not found, free the iterator and return null
	free(iter);
	return NULL;
}

/**
 * Mehotd takes in a pointer to a wordList and a character pointer
 * @param list - Pointer to word list structure
 * @param index
 * @return - wordData structure at specified index
 */
struct wordData* getWord(struct wordList * list, uint32_t index) {

	//ensure parameters arent null
	if(list == NULL || index<0 || index>list->words->size) {
		return NULL;
	}

	//return the wordData structure at index of list
	return ll_get(list->words, index);
}

/**
 * Method takes in a wordList pointer and searches through it for the wordData structure
 * containing a word that is also passed in by a char pointer then returns the count for
 * the word
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to find
 * @return
 */
uint32_t getWordCount(struct wordList * list, char * word) {

	//if either the word list or the word is null return -1 indicating an error
	if(list == NULL || word == NULL) {
		return -1;
	}

	//find structure containing specified word
	struct wordData* wordData = findWord(list, word);

	//if word was found then return the count else return 0
	if(wordData != NULL) {
		return wordData->count;
	}
	else {
		return 0;
	}
}

/**
 * Method frees all memory that is used by a wordList
 * @param list - Pointer to word list structure
 */
void cleanup(struct wordList * list) {

	//ensure list isnt null
	if(list != NULL) {

		//clear list (deallocate memory
		ll_clear(list->words);

		//decallocate list
		free(list->words);
	}
}

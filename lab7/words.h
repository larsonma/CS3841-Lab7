/*
 * words.h
 *
 * Header File for words.c
 *
 *  Created on: Sep 26, 2017
 *      Author: Gunther Huebler
 */

#ifndef WORDS_H_
#define WORDS_H_

#include <stdint.h>

#define WORD_LENGTH (32) //max length allowed for word

//contains a word and a counter for how often that word occurs
struct wordData {
	uint32_t count;
	char word[WORD_LENGTH+1];
};

//stucture for a list of words and a count of the total words contained
struct wordList {
	struct linkedList * words;
	uint32_t wordCount;
};

/**
 * Method takes in a pointer to a word list and initializes it by allocating space
 * for the linked list within it then setting the word count to 0
 * @param list - Pointer to word list structure
 */
void initWordList(struct wordList*);

/**
 * Method takes in a word list and pointer to word and adds the word to the list
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to add
 */
void addWord(struct wordList *, char *);

/**
 * Method takes a pointer to a list and a character pointer and returns the wordData structure
 * located in the list which contains the word defined by the character pointer
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to find the data structure of
 * @return - wordData structure for specified word
 */
struct wordData* findWord(struct wordList *, char *);

/**
 * Mehotd takes in a pointer to a wordList and a character pointer
 * @param list - Pointer to word list structure
 * @param index
 * @return - wordData structure at specified index
 */
struct wordData* getWord(struct wordList *, uint32_t);

/**
 * Method takes in a wordList pointer and searches through it for the wordData structure
 * containing a word that is also passed in by a char pointer then returns the count for
 * the word
 * @param list - Pointer to word list structure
 * @param word - Pointer to word to find
 * @return
 */
uint32_t getWordCount(struct wordList *, char *);

/**
 * Method frees all memory that is used by a wordList
 * @param list - Pointer to word list structure
 */
void cleanup(struct wordList *);


#endif /* WORDS_H_ */

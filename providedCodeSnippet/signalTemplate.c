/* The follwoing file contains a signal template that you can use
   as a signal template to handle a segmentation fault.
   Follow the isntructions to use it appropriately.
  Author: W. Schilling
  */
#include <signal.h>
#include "signalTemplate.h"
/**
 * This method is the signal handler that will handle the SIGSEGV segmentation fault signal.  It will 
 * indicate that the process attempted to access memory which was out of range.
 * @param signum This is the number of the signal.
 */
void sigsegvhandler(int signum) {
	// Indicate that an error occurred referencing memory.
	printf("Error: Attempting access memory that is out of range for process %d\n", getpid());

	// Now go through and unblock the signal so it can be sent again.
	// Start by defining a signal set.
	sigset_t x;

	// Initialize the signal set to be empty.  Then add the SIGSEGV signal to the set.
	sigemptyset(&x);
	sigaddset(&x, SIGSEGV);

	// Unblock the signal that caused us to execute this signal handler so that it can be called again.
	sigprocmask(SIG_UNBLOCK, &x, NULL);

	// Now jump back to a safe place before the erroneous dereference occurred.
	longjmp(restartEnvironment, 1);
}




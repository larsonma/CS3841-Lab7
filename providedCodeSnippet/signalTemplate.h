#ifndef SIGNALTEMPLATE_H
#define SIGNALTEMPLATE_H

/**
 * This method is the signal handler that will handle the SIGSEGV segmentation fault signal.  It will 
 * indicate that the process attempted to access memory which was out of range.
 * @param signum This is the number of the signal.
 */
void sigsegvhandler(int signum);

#endif



